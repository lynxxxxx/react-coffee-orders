import React, { useState, useEffect} from 'react'
import  { useForm } from 'react-hook-form'
import { storageSave } from '../../utils/storage'
import { loginUser } from './../../api/user'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 3
}

//Hooks
const LoginForm = () => {
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm()

    const {user, setUser} = useUser()
    const navigate = useNavigate()

    //Local state
    const [loading, setLoading] = useState(false)
    const [ apiError, setApiError] = useState(null)

    //Side effects
    useEffect(() => {
        //redirect to profile
        if (user !== null) {
            navigate('profile')
        }
        console.log("user changed! ", user)
      
    }, [user, navigate])//tom array menas only runs one

    //Event Handlers
    const onSubmit = async ({ username }) => {
        setLoading(true)
       const [error, userResponse] = await loginUser(username)//error -string för wrong och null if nothing went wrong/ user null för wrong and object för true
       if(error !==null){
        setApiError(error)
       }
       if (userResponse !== null) {
        storageSave(STORAGE_KEY_USER, userResponse)
        setUser(userResponse)
       }
       setLoading(false)
    }
  

    //Render Functions
    const errorMessage = (() => {
        if (!errors.username){
            return null
        }
        if (errors.username.type === 'required'){
            return <span>Username is required</span>
        }
        if (errors.username.type === 'minLength'){
            return <span>Username is to short(min 3)</span>
        }
    })()  // <= extra parantes här och i hela functionen gör att jag behöver inte paranter på function längre ner
    return ( 
        <>
        <h2>What your name</h2>
        {/* onSubmit i form är inte detsamma so onSubmit i function */}
        <form onSubmit={handleSubmit(onSubmit)}>
         <fieldset>
            <label htmlFor='username'>Username: </label>
            <input 
            type="text" 
            placeholder='name'
            {...register("username", usernameConfig) } 
            />
            {errorMessage}  
            {/* { (errors.username && errors.username.type === 'required') && <span>Username is required</span>}
            { (errors.username && errors.username.type === 'minLength') && <span>Username is to short(min 3)</span>} */}
         </fieldset>
         <button type='submit' disabled={ loading }>Continue</button>{/*false position vid click turn on to true*/}
         {loading && <p>Logging in ...</p>}{/*on true få ut p tag*/}
         { apiError && <p>{ apiError }</p> }
        </form>
        </>
     );
}
 
export default LoginForm;