export const storageSave = (key, value) => {
localStorage.setItem(key, JSON.stringify(value))//localStorage value förstår bara string
}

export const storageRead = key => {
    const data = localStorage.getItem(key)
    if (data ) {
        return JSON.parse(data)// from string to object
    }
    return null
}
 export const storageDelete = key => {
    localStorage.removeItem(key)
 }