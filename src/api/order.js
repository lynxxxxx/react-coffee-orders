import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL

export const orderAdd = async (user, order) => {

    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',//when you only need to update one field of the resource/ update existing data
            headers: createHeaders(),
            body: JSON.stringify({
                orders: [...user.orders, order]//sprida orders och lägga ny order
            })
        })

        if (!response.ok){
            throw new Error('Could not update the order')
        }

        const result = await response.json()
        return [null, result]//null if no error message
    }
    catch (error) {

    return[error.message, null]
    }
}


export const orderClearHistory = () => {

}