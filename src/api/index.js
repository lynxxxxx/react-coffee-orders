const apiKey = process.env.REACT_APP_API_KEY //we need to read key to create new user in db

export const createHeaders = () => {
    //this need to return new object
  return {
    'Content-Type': 'application/json',
    'x-api-key': apiKey
  }
}