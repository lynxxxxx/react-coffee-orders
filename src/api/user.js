import { createHeaders } from "./index";
const apiUrl = process.env.REACT_APP_API_URL;

const checkForUser = async (username) => {
  //check if user exist
  try {
    const response = await fetch(`${apiUrl}?username=${username}`);
    if (!response.ok) {
      throw new Error("Could not complete request.");
    }
    const data = await response.json(); //om user exist sparas den i data
    return [null, data]; //two argument "or"
  } catch (error) {
    return [error.message, []]; //two argument betyder or
  }
};

const createUser = async (username) => {
  //create a new user
  try {
    const response = await fetch(apiUrl, {
      //this need authentication
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        //vi kan inte fetcha object, måste konventera from object till string
        //http not undestand script object only string/
        username,
        orders: [],
      }),
    });
    if (!response.ok) {
      throw new Error("Could not create user with name." + username);
    }
    const data = await response.json();
    return [null, data]; //two argument "or"
  } catch (error) {
    return [error.message, []]; //two argument betyder or
  }
};

export const loginUser = async (username) => {
  const [checkError, user] = await checkForUser(username);

  if (checkError !== null) {
    // if something is wrong return this
    return [checkError, null];
  }

  if (user.length > 0) {
    //user exist
    return [null, user.pop()];
  }
  //user does not exist
  return await createUser(username);
};

//detta här hjälper mig att updatera ny lagda order på weben så att man ser den
export const userById = async (userId) => {
  // värje gång man gör async vi need to do try catch
  try {
    const response = await fetch(`${apiUrl}/${userId}`);

    if (!response.ok) {
      throw new Error("Could not fetch user");
    }
    const user = await response.json();
    return [null, user]
  } catch (error) {
    return [error.message, null]
  }
};

//[error, user]expected första returnerar if det error andra if not error
