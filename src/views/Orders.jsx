import { useState } from "react";
import { orderAdd } from "../api/order";
import OrdersCoffeeButton from "../components/Orders/OrdersCoffeeButton";
import OrdersForm from "../components/Orders/OrdersForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const COFFEES = [
  {
    id: 1,
    name: "Americano",
    image: "img/hot-coffee.png",
  },
  {
    id: 2,
    name: "Cappuccino",
    image: "img/coffee.png",
  },
  {
    id: 3,
    name: "Latte",
    image: "img/latte.png",
  },
  {
    id: 4,
    name: "Espresso",
    image: "img/hot-coffee.png",
  }
    
]

const Orders = () => {

  const [coffee, setCoffee] = useState(null)
  const {user, setUser} = useUser()

    const handleCoffeeClicked = (coffeeId) => {
      console.log(coffeeId)
     // const selectedCoffee = COFFEES.find(coffee => coffee.id === coffeeId)
      setCoffee(COFFEES.find(coffee => coffee.id === coffeeId))

    }

    const handleOrderClick = async (notes) => {
      console.log(notes)
      //check if we have a coffee
      if(!coffee) {
        alert('Please select a coffee first')
        return
      }
      //combine the coffee with the notes
      const order = (coffee.name + ' ' + notes).trim()
      console.log(order)
      // send an http request
      const [error, updateUser] = await orderAdd(user, order)
      if (error !== null) {
        //bad
        return
       }
       //keep ui state and server state in sync
       storageSave(STORAGE_KEY_USER, updateUser)
       
       //update context state
       setUser(updateUser)
      console.log('Error ', error)
      console.log('updateUser ', updateUser)

    }


    const availableCoffees = COFFEES.map(coffee => {
      return <OrdersCoffeeButton
       key={coffee.id}
        coffee={coffee} 
        onSelect={handleCoffeeClicked}
         />
    })

    return ( 
        <>
            <h1>Orders</h1>
            <section id="coffee-options">
              {/* <OrdersCoffeeButton name="Americano" image="img/hot-coffee.png" key="americano"/>
              <OrdersCoffeeButton name="Cappuccino" image="img/coffee.png" key="cappuccino"/>
              <OrdersCoffeeButton name="Latte" image="img/latte.png" key="latte" />
              <OrdersCoffeeButton name="Espresso" image="img/hot-coffee.png" key="espresso"/> */}
              { availableCoffees }
            </section>
            <section id="order-notes">
             <OrdersForm onOrder={handleOrderClick}/>
            </section>
            <p>Summary:</p>
            {coffee && <p>Selected coffee: {coffee.name}</p>}
        </>
    
     );
}
 
export default withAuth(Orders);